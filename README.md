Buildy 2.0
================================================================================

Internal web tool for searching and kicking off Jenkins builds.

Core libraries: React, Express/Node, RethinkDB, SocketIO, Parcel.


Initial Setup
--------------------------------------------------------------------------------
*May require installing node, nodemon, npm, nvm

`$ cd <root of repo>`

`$ nvm use`

`$ npm install`

Development
--------------------------------------------------------------------------------
`$ npm start`

This will bundle the front end code and kick off a server listening at `localhost:1234`.
Server side code changes will restart the server using nodemon. Client-side changes
will cause parcel to reload the necessary React components using HMR. If nodemon
has restarted the server since you loaded the webpage, you will need to reload the
webpage for HMR to work properly. This script includes the node `--inspect` option
which will spawn a chrome debugger for the server-side code.

Production
--------------------------------------------------------------------------------
`$ npm run prod`

This will bundle all the code into `/dist/prod` and will kick off a server listening
at `localhost:4568`. This server/client will not restart/reload from any code changes.


NOTE: Auth0 requires a list of allowed callback URLs that are safe to navigate
to after logging in is complete. If you attempt to authenticate from a url/port
other than localhost|lnx-terryc|lnx-terryc.vaddio.com & 1234|4568, Auth0 will fail.
