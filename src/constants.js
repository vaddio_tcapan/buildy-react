// -----------------------------------------------------------------------------
// Constants
// -----------------------------------------------------------------------------
export const TYPES = [
  'all',
  'arm',
  'ark',
  'test'
];

export const TYPES_MAP = {
  all: 'All',
  arm: 'ARM',
  ark: 'Ark',
  test: 'Test'
};

export const INPUT_FILTER_KEY = 'buildy-input-filter';

export const TYPE_FILTER_KEY = 'buildy-type-filter';

export const PARAMETERS_KEY = 'buildy-parameters';

export const USER_PARAMETERS_KEY = 'buildy-user-parameters';

export const USER_DATA_KEY = 'buildy-user-data';

export const AUTH_PROFILE_KEY = 'buildy-profile';

export const AUTH_ACCESS_TOKEN_KEY = 'buildy-access-token';

export const JENKINS_HOME_PAGE = 'http://jenkins.vaddio.com/jenkins';
