import Auth0Lock from 'auth0-lock';
import axios from 'axios';
import _ from 'underscore';
import { AUTH_ACCESS_TOKEN_KEY, AUTH_PROFILE_KEY, USER_DATA_KEY } from '../constants';

// -----------------------------------------------------------------------------
// Constants
// -----------------------------------------------------------------------------
const LOCK_DOMAIN = 'jenkins-web.auth0.com';
const LOCK_CLIENT_ID = 'NWhikCYisEZUKGe1UQYIAwVer6u8wAuZ';

const USER_PROFILE_KEYS = [
  'name',
  'picture',
  'nickname',
  'email',
  'jenkins_username',
  'jenkins_api_token'
];

const USER_ID_PROFILE_ALIAS = 'sub';


// -----------------------------------------------------------------------------
// Local
// -----------------------------------------------------------------------------
function clearProfile() {
  localStorage.removeItem(AUTH_PROFILE_KEY);
}

function clearAccessToken() {
  localStorage.removeItem(AUTH_ACCESS_TOKEN_KEY);
}

function clearUserData() {
  localStorage.removeItem(USER_DATA_KEY);
}

// -----------------------------------------------------------------------------
// AUTH
// -----------------------------------------------------------------------------
function initAuthLock() {
  return new Auth0Lock(LOCK_CLIENT_ID, LOCK_DOMAIN, {
    autoclose: true,
    auth: {
      responseType: 'token',
      redirect: false,
      params: {
        audience: 'https://jenkins-web.auth0.com/api/v2/',
        scope: 'openid profile email',
        client_id: LOCK_CLIENT_ID
      }
    }
  });
}

function login(lock) {
  return lock.show();
}

function logout() {
  clearProfile();
  clearAccessToken();
  clearUserData();
}

function getProfile() {
  return JSON.parse(localStorage.getItem(AUTH_PROFILE_KEY));
}

function getUserID() {
  const profile = getProfile();
  return profile && profile.sub;
}

function setProfile(profile) {
  return localStorage.setItem(AUTH_PROFILE_KEY, profile);
}

function isLoggedIn() {
  return !!localStorage.getItem(AUTH_PROFILE_KEY);
}

function getAccessToken() {
  return JSON.parse(localStorage.getItem(AUTH_ACCESS_TOKEN_KEY));
}

// Get and store access_token in local storage
function setAccessToken(accessToken) {
  localStorage.setItem(AUTH_ACCESS_TOKEN_KEY, accessToken);
}

function dbSaveOrUpdateUser(authProfile) {
  const user = _.pick(authProfile, USER_PROFILE_KEYS);

  if (authProfile[USER_ID_PROFILE_ALIAS]) {
    user.user_id = authProfile[USER_ID_PROFILE_ALIAS];
  }

  return axios.post('/api/user/login', user);
}

// -----------------------------------------------------------------------------
// Exports
// -----------------------------------------------------------------------------
export default {
  AUTH_PROFILE_KEY,
  AUTH_ACCESS_TOKEN_KEY,
  USER_PROFILE_KEYS,
  initAuthLock,
  login,
  logout,
  getProfile,
  getUserID,
  setProfile,
  isLoggedIn,
  getAccessToken,
  setAccessToken,
  dbSaveOrUpdateUser
};
