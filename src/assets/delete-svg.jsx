/* eslint max-len: ["error", { "code": 10000 }] */

import React from 'react';

export default function DeleteSvg() {
  return (
    <svg
      className="delete-svg"
      xmlns="http://www.w3.org/2000/svg"
      width="50"
      height="50"
      viewBox="0 0 50 50"
      style={{
        fill: 'rgb(128, 128, 128)'
      }}
    >
      <path
        style={{
          textIndent: 0,
          textAlign: 'start',
          lineHeight: 'normal',
          textTransform: 'none',
          BlockProgression: 'tb',
          '-inkscape-font-specification': 'Bitstream Vera Sans'
        }}
        d="M 9.15625 6.3125 L 6.3125 9.15625 L 22.15625 25 L 6.21875 40.96875 L 9.03125 43.78125 L 25 27.84375 L 40.9375 43.78125 L 43.78125 40.9375 L 27.84375 25 L 43.6875 9.15625 L 40.84375 6.3125 L 25 22.15625 L 9.15625 6.3125 z"
        color="#000"
        overflow="visible"
        enableBackground="accumulate"
        fontFamily="Bitstream Vera Sans"
      />
    </svg>
  );
}
