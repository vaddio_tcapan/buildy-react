import React from 'react';
import helpers from '../../helpers';

const ButtonToggle = (props) => {
  const {
    className,
    onClick,
    name,
    isToggledOn
  } = props;

  const selected = isToggledOn ? 'selected' : '';

  return (
    <li>
      <button
        className={`${className} ${selected}`}
        onClick={onClick}
        value={name}
      >
        {helpers.machineToTitle(name)}
      </button>
    </li>
  );
};

export default ButtonToggle;
