import React, { Component } from 'react';

class SelectInput extends Component {
  static getDerivedStateFromProps(nextProps, prevState) {
    const value = nextProps.value;

    if (value !== prevState.value) {
      return {
        value
      };
    }
    return null;
  }

  state = {
    value: this.props.value
  }

  onChange = (event) => {
    const value = event.target.value;
    this.setState({ value });
    this.props.onInputChange(value, this.props.name);
  }

  render() {
    return (
      <select
        className={this.props.className}
        type="text"
        placeholder={this.props.placeholder}
        value={this.state.value}
        onChange={this.onChange}
        title={this.props.title}
        name={this.props.name}
      >
        {this.props.options.map(option => <option key={option} value={option}>{option}</option>)}
      </select>
    );
  }
}

export default SelectInput;
