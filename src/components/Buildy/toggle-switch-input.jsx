import React, { Component } from 'react';

class ToggleSwitchInput extends Component {
  static getDerivedStateFromProps(nextProps, prevState) {
    const value = nextProps.value;
    const is_boolean = typeof value === 'boolean';

    if (is_boolean && value !== prevState.value) {
      return {
        value
      };
    }
    return null;
  }

  constructor(props) {
    super(props);

    this.state = {
      value: typeof this.props.value === 'boolean' ? this.props.value : false
    };
  }

  onChange = (event) => {
    const value = event.target.checked;
    this.setState({ value });
    this.props.onInputChange(value, this.props.name);
  }

  render() {
    return (
      <div className="flexbox-wrapper">
        <label className="checkbox-wrapper" title={this.props.title}>
          <input
            className="checkbox-hidden"
            type="checkbox"
            checked={this.state.value}
            onChange={this.onChange}
          />
          <span className="checkbox-toggle" />
        </label>
      </div>
    );
  }
}

export default ToggleSwitchInput;
