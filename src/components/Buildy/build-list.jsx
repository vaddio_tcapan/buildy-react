/* eslint no-bitwise: ["error", { "allow": ["~"] }] */
import React, { Component } from 'react';
import socket from '../../socket-client';
import Build from './build';
import helpers from '../../helpers';
import NewmanText from './newman-text';
import giantJenkins from '../../assets/giant-jenkins.png';

class BuildList extends Component {
  state = {
    builds: [],
    polishing: false,
    started_polishing: 0
  }

  componentDidMount() {
    socket.on('build', this.updateBuilds);

    // Show loading message for a minumum amount of time if builds haven't arrived yet.
    if (!this.state.builds.length) {
      const start_time = new Date();
      this.setState({
        polishing: true,
        started_polishing: start_time
      });
    }
  }

  shouldComponentUpdate() {
    if (!this.state.polishing) return true;

    const elapsed_time = new Date() - this.state.started_polishing;
    const time_left = 1200 - elapsed_time;

    if (time_left <= 0) {
      this.setState({
        polishing: false,
        started_polishing: 0
      });
      return true;
    }

    setTimeout(() => {
      this.setState({
        polishing: false,
        started_polishing: 0
      });
    }, time_left);

    return false;
  }

  componentWillUnmount() {
    socket.off('build');
  }

  updateBuilds = (action, data) => {
    const builds = this.state.builds;
    let build_idx;

    switch (action) {
      case 'ALL':
        this.setState({ builds: data });
        // helpers.syncCssAnimations();
        break;
      case 'DELETE':
        build_idx = builds.findIndex(build => build.id === data.id);
        if (build_idx > -1) {
          builds.splice(build_idx, 1);
          this.setState({ builds });
        }
        break;
      case 'INSERT':
        builds.push(data);
        this.setState({ builds });
        // helpers.syncCssAnimations();
        break;
      case 'REPLACE':
        build_idx = builds.findIndex(build => build.id === data.id);
        if (build_idx > -1) {
          builds[build_idx] = data;
          this.setState({ builds });
        }
        // TODO: status change notifications? Probably only worth doing if desktop notifications
        // are working. Which, at the moment, don't seem to be on any browser. Chrome
        // blocking the notification api for non https connections?
        break;
      default:
        break;
    }
  }

  // Use arrow function to bind to outer 'this'. Comes with 'stage-2' or 'transform-class-properties' babel feature.
  filterBuild = (build) => {
    const userName = build.userName;
    const commit = build.COMMIT;
    const userId = build.userId;
    const description = build.description;
    const productVersion = build.VAD_PRODUCT_VERSION;
    const machine = build.MACHINE;
    const sha = build.SHA;
    const inputs = this.props.inputFilter.toLowerCase().split(' ');
    const type = build.type;
    const date = helpers.getDate(build.timestamp, true);

    const type_filter = this.props.typeFilter;
    if (type_filter !== 'all' && type !== type_filter) return false;

    return inputs.reduce((memo, input) => {
      const userNameCheck = userName ? ~userName.toLowerCase().indexOf(input) : false;
      const userIdCheck = userId ? ~userId.toLowerCase().indexOf(input) : false;
      const commitCheck = commit ? ~commit.toLowerCase().indexOf(input) : false;
      const descriptionCheck = description ? ~description.toLowerCase().indexOf(input) : false;
      const productVersionCheck = productVersion ? ~productVersion.toLowerCase().indexOf(input) : false;
      const machineCheck = machine ? ~machine.toLowerCase().indexOf(input) : false;
      const shaCheck = sha ? ~sha.indexOf(input) : false;
      const typeCheck = type ? ~type.indexOf(input) : false;
      const dateCheck = date ? ~date.indexOf(input) : false;

      if (!(userIdCheck
        || userNameCheck
        || commitCheck
        || descriptionCheck
        || productVersionCheck
        || machineCheck
        || shaCheck
        || typeCheck
        || dateCheck
      )) {
        return false;
      }
      return memo;
    }, true);
  }

  sortBuilds = (a, b) => {
    const aTime = a.timestamp;
    const bTime = b.timestamp;

    if (aTime === bTime) return 0;
    if (aTime > bTime) return -1;
    return 1;
  }

  render() {
    if (this.state.builds.length) {
      const filtered_builds = this.state.builds.filter(this.filterBuild);
      const sorted_builds = filtered_builds.sort(this.sortBuilds);

      if (!sorted_builds.length) return NewmanText(`Ah ah ah. Your query doesn't match any builds.`);

      return (
        <ul>
          {sorted_builds.map(build => <Build key={build.id} build={build} />)}
        </ul>
      );
    }

    return NewmanText('Polishing builds, Your Grace.', giantJenkins, 'giant-jenkins');
  }
}

export default BuildList;
