// -----------------------------------------------------------------------------
// imports
// -----------------------------------------------------------------------------
import React, { Component } from 'react';
import _ from 'underscore';
import PNotify from 'pnotify/dist/es/PNotifyCompat';
import 'pnotify/dist/es/PNotifyCallbacks';
import 'pnotify/dist/es/PNotifyButtons';

import auth from '../../Auth/Auth';
import TextInput from './text-input';

// -----------------------------------------------------------------------------
// Local
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Component
// -----------------------------------------------------------------------------
class SaveJenkinsForm extends Component {
  state = {
    jenkins_api_token: '',
    jenkins_username: '',
    notification: ''
  }

  onChildChange = (value, name) => {
    const state = { ...this.state };
    state[name] = value;
    this.setState(state);
  }

  handleSubmit = (event) => {
    _.result(event, 'preventDefault');
    const user_profile = auth.getProfile();
    const jenkins_data = this.state;
    _.extend(user_profile, jenkins_data);

    auth.dbSaveOrUpdateUser(user_profile).then((res) => {
      const user = res.data;
      if (!user.has_valid_jenkins_auth) {
        this.renderInvalidDataPNotify();
      }

      this.props.userContext.updateUserData(user);
    });
  }

  renderInvalidDataPNotify() {
    const self = this;
    let notification;

    if (!this.state.notification) {
      notification = new PNotify({
        title: 'Invalid Jenkins Data',
        type: 'error',
        text: `\
          <div class="filter">\
            Find your Jenkins API Token and User ID on\
            <a href="http://jenkins.vaddio.com" target="_blank"> Jenkins </a>\
            under the "Configure" option of the upper right menu.\
            Unless you change your API Token, you will only need to enter these once.\
          </div>\
        `,
        afterClose() {
          self.setState({ notification: '' });
        },
        buttons: {
          closerHover: true,
          sticker: false
        }
      });

      this.setState({ notification });
    }
  }

  render() {
    return (
      <form
        className="save-jenkins"
        onSubmit={this.handleSubmit}
        title={`This info is found on Jenkins under the "Configure" option of the upper right menu.`}
        autoComplete="off"
      >
        <TextInput
          value={this.state.jenkins_username}
          name="jenkins_username"
          onInputChange={this.onChildChange}
          placeholder="Jenkins User ID"
        />
        <TextInput
          value={this.state.jenkins_api_token}
          name="jenkins_api_token"
          onInputChange={this.onChildChange}
          placeholder="Jenkins API Token"
        />
        <input className="submit-input button" type="submit" value="Save Credentials" />
      </form>
    );
  }
}


// -----------------------------------------------------------------------------
// exports
// -----------------------------------------------------------------------------
export default SaveJenkinsForm;
