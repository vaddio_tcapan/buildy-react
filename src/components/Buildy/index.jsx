// -----------------------------------------------------------------------------
// Imports
// -----------------------------------------------------------------------------
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import _ from 'underscore';
import './styles.css';
import auth from '../../Auth/Auth';
import BuildList from './build-list';
import TextInput from './text-input';
import BuildForm from './build-form';
import { TYPES, INPUT_FILTER_KEY, TYPE_FILTER_KEY } from '../../constants';
import UserContext from '../../contexts/user-context';
import SaveJenkinsForm from './save-jenkins-form';
import NewmanText from './newman-text';

// -----------------------------------------------------------------------------
// Constants
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// View
// -----------------------------------------------------------------------------
/**
 * View for rendering builds, build filter, build forms
 */
@withRouter
class Buildy extends Component {
  static getDerivedStateFromProps(nextProps, prevState) {
    let type_filter = nextProps.match.params.type || localStorage.getItem(TYPE_FILTER_KEY);

    let input_filter = decodeURIComponent(nextProps.location.search).replace(/^\?/, '');

    type_filter = TYPES.includes(type_filter) ? type_filter : _.first(TYPES);
    input_filter = input_filter || localStorage.getItem(INPUT_FILTER_KEY) || '';

    if (type_filter !== prevState.typeFilter || input_filter !== prevState.inputFilter) {
      return {
        typeFilter: type_filter,
        inputFilter: input_filter
      };
    }
    return null;
  }

  constructor(props) {
    super(props);

    this.state = {
      typeFilter: localStorage.getItem(TYPE_FILTER_KEY) || _.first(TYPES),
      inputFilter: this.getInitialInputFilter()
    };
  }

  componentDidUpdate() {
    const { history, location } = this.props;
    const type_filter = this.state.typeFilter;
    const input_filter = this.state.inputFilter;
    const encoded_input_filter = encodeURIComponent(input_filter);

    const pathname = type_filter ? `/${type_filter}` : '';
    const search = input_filter ? `?${encoded_input_filter}` : '';

    if (search !== location.search) {
      history.replace(`${pathname}${search}`);
    }

    localStorage.setItem(INPUT_FILTER_KEY, input_filter);
    localStorage.setItem(TYPE_FILTER_KEY, type_filter);
  }

  onFilterChange = _.debounce((filter) => {
    this.updateInputFilter(filter);
  }, 1000)

  getInitialInputFilter = () => {
    const url_search = decodeURIComponent(this.props.location.search).replace(/^\?/, '');
    const previous_filter = localStorage.getItem(INPUT_FILTER_KEY);
    return url_search || previous_filter || '';
  }

  updateInputFilter(filter) {
    if (filter === this.state.filter) return;
    this.setState({ inputFilter: filter });
  }

  render() {
    return (
      <UserContext.Consumer>
        {(userContext) => {
          const user = userContext.user_data;
          const loggedIn = auth.isLoggedIn();
          let Building;

          if (loggedIn && user && user.has_valid_jenkins_auth) {
            Building = <BuildForm userContext={userContext} />;
          } else if (loggedIn && user && user.user_id) {
            Building = (
              <div>
                <SaveJenkinsForm userContext={userContext} />
                {NewmanText('Ah ah ah. You havent saved your Jenkins API credentials.')}
              </div>
            );
          } else {
            Building = NewmanText(`Ah ah ah. You're not logged in.`);
          }

          return ([
            <div key="build-list" className="build-list">
              <TextInput
                value={this.state.inputFilter}
                onInputChange={this.onFilterChange}
                className="build-filter"
                placeholder="Filter Builds (e.g. night integ, helios rc12, <SHA1>)"
                title="Search for multiple build parameters separated by spaces"
              />
              <BuildList
                inputFilter={this.state.inputFilter}
                typeFilter={this.state.typeFilter}
              />
            </div>,
            <div key="build-form-title" className="build-form-title">
              Build Creation
            </div>,
            <div key="build-forms" className="build-forms">
              {Building}
            </div>
          ]);
        }}
      </UserContext.Consumer>
    );
  }
}


// -----------------------------------------------------------------------------
// Exports
// -----------------------------------------------------------------------------
export default Buildy;
