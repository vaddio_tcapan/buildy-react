import React, { Component } from 'react';
import _ from 'underscore';
import ToolTip from '../lib/tooltip';
import helpers from '../../helpers';
import LockSvg from '../../assets/lock-svg';


const TOOLTIP_TITLE_MAP = {
  VNG_REPO: 'Repo',
  COMMIT: 'Commit',
  VAD_PRODUCT_VERSION: 'Product Version',
  description: 'Description',
  SHA: 'SHA',
  url: 'URL'
};


const TOOLTIP_STYLE = {
  style: {
    padding: '8px',
    backgroundColor: '#44867C',
    color: '#FF8BC0',
    boxShadow: 'none',
    border: '1px solid #FF8BC0'
  },
  arrowStyle: {
    color: '#FF8BC0',
    borderColor: false
  }
};


class Build extends Component {
  // componentDidUpdate(prevProps) {
  //   const status_changed = prevProps.build.result !== this.props.build.result;

  //   if (status_changed) {
  //     helpers.syncCssAnimations();
  //   }
  // }

  state = {
    isTooltipActive: false
  }

  getBuildUrl = (artifact) => {
    const build = this.props.build;
    return `${build.url}artifact/${artifact.relativePath}`;
  }

  getTooltipMarkup = () => {
    const build = this.props.build;
    const sort_order = _.keys(TOOLTIP_TITLE_MAP);
    const filtered_keys = sort_order.filter(key => build[key]);

    return _.map(filtered_keys, key => (
      <div key={key}>
        <span className="tooltip-title">{`${TOOLTIP_TITLE_MAP[key]}`}</span>
        <span className="tooltip-content">{build[key]}</span>
      </div>
    ));
  }

  getDownloadsMarkup = () => {
    const build = this.props.build;

    if (build.artifacts && build.artifacts.length) {
      return build.artifacts.map(artifact => (
        <div key={artifact.fileName}>
          <a href={this.getBuildUrl(artifact)}>
            {artifact.fileName}
          </a>
        </div>
      ));
    }

    return '';
  }

  hideTooltip = () => {
    this.setState({ isTooltipActive: false });
    // console.log('hide');
  }

  showTooltip = () => {
    this.setState({ isTooltipActive: true });
    // console.log('show');
  }

  handleFirmwareClick = (e, data) => {
    _.result(e, 'preventDefault');
    const url = this.getBuildUrl(data.artifact);
    window.open(url);
  }

  render() {
    const build = this.props.build;
    const status = build.building ? 'BUILDING' : build.result;
    let download_links;
    let downloads_placeholder = 'No firmware downloads available';

    if (build.building) downloads_placeholder = 'Build in progress';

    if (build.artifacts && build.artifacts.length) {
      download_links = this.getDownloadsMarkup();
    }

    return (
      <li>
        <a
          id={`tooltip-${build.id}`}
          className={`build-item ${status}`}
          href={build.url}
          target="_blank"
          onMouseEnter={this.showTooltip}
          onMouseLeave={this.hideTooltip}
        >
          <div className="builder">{build.userName}</div>
          <div className="timestamp">{helpers.getDate(build.timestamp)}</div>
          <div className="machine">
            {build.keepLog ? LockSvg() : ''}
            {helpers.machineToTitle(build.MACHINE)}
          </div>
          <div className="version-commit">{build.VAD_PRODUCT_VERSION || build.COMMIT}</div>
        </a>

        <ToolTip
          active={this.state.isTooltipActive}
          position="right"
          arrow="center"
          parent={`#tooltip-${build.id}`}
          style={TOOLTIP_STYLE}
        >
          <div>
            {this.getTooltipMarkup()}
            <div className="download-links">
              {/* <div className="tooltip-title">Firmware Downloads</div> */}
              {download_links || downloads_placeholder}
            </div>
          </div>
        </ToolTip>
      </li>
    );
  }
}

export default Build;
