import React, { Component } from 'react';
import _ from 'underscore';
import ButtonToggle from './button-toggle';

class ButtonMultiSelect extends Component {
  static getDerivedStateFromProps(nextProps, prevState) {
    const value = nextProps.value;

    if (value !== prevState.value) {
      return {
        selected: value
      };
    }
    return null;
  }

  state = {
    selected: this.props.value
  }

  onChildToggle = (event) => {
    _.result(event, 'preventDefault');
    const child = event.target.value;
    const selected = this.state.selected;
    const isToggledOn = selected.includes(child);

    if (isToggledOn) {
      const child_idx = selected.indexOf(child);
      if (child_idx > -1) {
        selected.splice(child_idx, 1);
      }
    } else {
      selected.push(child);
    }

    this.setState({ selected });
    this.props.onInputChange(selected, this.props.name);
  }

  clearAll = (event) => {
    _.result(event, 'preventDefault');
    const selected = [];
    this.setState({ selected });
    this.props.onInputChange(selected, this.props.name);
  }

  render() {
    const selected = this.state.selected;

    return (
      <div className="multi-select-wrapper">
        <button className="button clear-all" onClick={this.clearAll}>Clear All</button>
        <ul className="multi-select-list">
          {this.props.options.map((option) => {
            const isToggledOn = Array.isArray(selected) ? selected.includes(option) : false;
            return (
              <ButtonToggle
                className="button-toggle button"
                key={option}
                name={option}
                isToggledOn={isToggledOn}
                onClick={this.onChildToggle}
              />
            );
          })}
        </ul>
      </div>
    );
  }
}

export default ButtonMultiSelect;
