// -----------------------------------------------------------------------------
// imports
// -----------------------------------------------------------------------------
import React, { Component } from 'react';
import _ from 'underscore';
import axios from 'axios';

// Use stable pnotify
import PNotify from 'pnotify/dist/es/PNotifyCompat';
import 'pnotify/dist/es/PNotifyButtons';

import socket from '../../socket-client';
import { PARAMETERS_KEY, USER_PARAMETERS_KEY } from '../../constants';
import TextInput from './text-input';
import helpers from '../../helpers';

import SelectInput from './select-input';
import ToggleSwitchInput from './toggle-switch-input';
import ButtonMultiSelect from './button-multi-select-input';
import NewmanText from './newman-text';
import giantJenkins from '../../assets/giant-jenkins.png';


// -----------------------------------------------------------------------------
// Constants
// -----------------------------------------------------------------------------
const PARAMETER_NAME_MAP = {
  PRODUCT: ButtonMultiSelect,
  MACHINE: ButtonMultiSelect
};

const PARAMETER_TYPE_MAP = {
  StringParameterDefinition: TextInput,
  ChoiceParameterDefinition: SelectInput,
  BooleanParameterDefinition: ToggleSwitchInput,
  // IMPROVE: pass a prop to use type password? Might not be necessary here.
  PasswordParameterDefinition: TextInput
};

const TEST_BUILD_TITLES = [
  'web',
  'python',
  'rest',
  'WEB_TEST',
  'PYTHON_TEST',
  'REST_TEST'
];

const BUILD_TYPE_SORT_MAP = [
  'common',
  'arm',
  'ark',
  'web',
  'python',
  'rest'
];

const BUILD_TYPE_TITLE_MAP = {
  common: '',
  arm: 'ARM',
  ark: 'Ark',
  web: 'Web Test',
  python: 'Python3 tox Test',
  rest: 'Rest Regression Test'
};

const PARAM_NAME_TITLE_MAP = {
  WEB_TEST: '',
  PYTHON_TEST: '',
  REST_TEST: '',
  DeviceType: 'Device Type',
  DeviceUser: 'Device User',
  DeviceIP: 'Device IP',
  PRODUCT: '',
  MACHINE: ''
};

// -----------------------------------------------------------------------------
// Local
// -----------------------------------------------------------------------------
function getJenkinsDefaultParameters(params) {
  const defaults = {};

  const flat_params = _.flatten(_.values(params));

  flat_params.forEach((param) => {
    defaults[param.name] = param.defaultParameterValue.value;
  });

  return defaults;
}

/**
 * Ideas for python build kicking off on up/master instead of commit. Could it
 * relate to what I've seen with a stagnant window seeming to have stale build data?
 *   - localstorage not being updated properly
 *   - a problem with shouldComponentUpdate logic
 *   - how i'm updating state from form elements, could some get missed?
 *
 */

// -----------------------------------------------------------------------------
// Component
// -----------------------------------------------------------------------------
class BuildForm extends Component {
  constructor(props) {
    super(props);

    const parameters = JSON.parse(localStorage.getItem(PARAMETERS_KEY)) || {};
    const jenkins_defaults = getJenkinsDefaultParameters(parameters);
    const cached_parameters = JSON.parse(localStorage.getItem(USER_PARAMETERS_KEY)) || {};
    const user_parameters = _.extend(jenkins_defaults, cached_parameters);

    this.state = {
      parameters,
      user_parameters,
      polishing: false,
      started_polishing: 0
    };
  }

  componentDidMount() {
    // Show loading message for a minumum amount of time if params haven't arrived yet.
    if (Object.keys(this.state.parameters).length === 0) {
      const start_time = new Date();
      this.setState({
        polishing: true,
        started_polishing: start_time
      });
    }

    // if items in localstorage go away (expire?) and we get new params here, it would set item in
    // localstorage with jenky defaults but form would still show previous items?
    // That makes sense except for it happening after recent submits because that sets the item also so
    // they shouldnt expire.
    socket.on('parameters', (parameters) => {
      const new_jenkins_defaults = getJenkinsDefaultParameters(parameters);
      const user_parameters = _.extend(new_jenkins_defaults, this.state.user_parameters);

      if (JSON.stringify(this.state.parameters) !== JSON.stringify(parameters)) {
        this.setState({ parameters });
      }
      localStorage.setItem(PARAMETERS_KEY, JSON.stringify(parameters));

      if (JSON.stringify(this.state.user_parameters) !== JSON.stringify(user_parameters)) {
        this.setState({ user_parameters });
      }
      localStorage.setItem(USER_PARAMETERS_KEY, JSON.stringify(user_parameters));
    });

    socket.emit('login');

    if (window.Notification.permission === 'default') {
      window.Notification.requestPermission();
    }
  }

  shouldComponentUpdate() {
    if (!this.state.polishing) return true;

    const elapsed_time = new Date() - this.state.started_polishing;
    const time_left = 1200 - elapsed_time;

    if (time_left <= 0) {
      this.setState({
        polishing: false,
        started_polishing: 0
      });
      return true;
    }

    setTimeout(() => {
      this.setState({
        polishing: false,
        started_polishing: 0
      });
    }, time_left);

    return false;
  }

  componentWillUnmount() {
    socket.off('parameters');
  }

  onChildChange = (value, name) => {
    const user_parameters = { ...this.state.user_parameters };
    let element;

    user_parameters[name] = value;
    this.setState({ user_parameters });

    // Toggle class for hiding params when tests are toggled off.
    if (TEST_BUILD_TITLES.includes(name)) {
      element = document.querySelector(`.${name}`);
      element.classList.toggle('test-off', !value);
    }
  }

  getParamsByType = (type, parameters) => {
    const defaults = this.state.user_parameters;
    const test_class = TEST_BUILD_TITLES.includes(type) ? 'test' : '';
    const heading = type === 'common'
      ? ''
      : <h2 className={`build-type ${test_class}`}>{BUILD_TYPE_TITLE_MAP[type]}</h2>;

    const params = parameters.map((param) => {
      const has_alternate_title = Object.prototype.hasOwnProperty.call(PARAM_NAME_TITLE_MAP, param.name);
      const name = has_alternate_title ? PARAM_NAME_TITLE_MAP[param.name] : param.name;
      const InputElement = PARAMETER_NAME_MAP[param.name] || PARAMETER_TYPE_MAP[param.type];

      let test_status;
      let include_test_class = '';

      if (TEST_BUILD_TITLES.includes(param.name)) {
        test_status = defaults[param.name] ? '' : 'test-off';
        include_test_class = `include-test ${test_status}`;
      }

      const autoComplete = param.name === 'Password' ? 'off' : 'on';

      return (
        <div className={`build-param ${param.name} ${include_test_class}`} key={param.name}>
          {name ? <label>{helpers.paramToTitle(name)}</label> : ''}
          <InputElement
            value={defaults[param.name]}
            title={param.description}
            options={param.choices}
            name={param.name}
            onInputChange={this.onChildChange}
            autoComplete={autoComplete}
          />
        </div>
      );
    });

    return <div key={type}>{heading}{params}</div>;
  }

  alertInvalidUser() {
    this.notification.update({
      title: 'Error',
      type: 'error',
      text: 'Server failed to find your Jenkins API credentials. Try logging out and logging in again.'
    });
  }

  alertBuildStatus(buildData) {
    let text = '';
    const successes = buildData.filter(build => build.created).map(build => build.build);
    const failures = buildData.filter(build => !build.created).map(build => build.build);

    if (successes.length) {
      const success_list = successes.reduce((memo, build) => `${memo}\n\u00A0\u00A0${helpers.machineToTitle(build)}`, '');
      text = text.concat(`CREATED BUILDS:${success_list}`);
      if (failures.length) text = text.concat('\n\n');
    }

    if (failures.length) {
      const fail_list = failures.reduce((memo, build) => `${memo}\n\u00A0\u00A0${helpers.machineToTitle(build)}`, '');
      text = text.concat(`FAILED TO CREATE:${fail_list}`);
    }

    this.notification.update({
      title: 'Build Status',
      type: 'info',
      text
    });
  }

  handleSubmit = (event) => {
    _.result(event, 'preventDefault');
    const user_params = this.state.user_parameters;

    localStorage.setItem(USER_PARAMETERS_KEY, JSON.stringify(user_params));

    user_params.user_id = this.props.userContext.user_data.user_id;

    // TODO: Check if any builds are actually toggled before sending anything.
    this.notification = new PNotify({
      type: 'info',
      title: 'Sending Builds',
      text: 'Contacting Jenkins...',
      addClass: 'build-creation',
      icon: false,
      hide: false,
      buttons: {
        sticker: false
      }
    });

    axios.post('/api/builds', user_params).then((response) => {
      const user = response.data.user;
      const builds = response.data.builds;
      if (user !== undefined) {
        this.props.userContext.updateUserData(user);
        this.alertInvalidUser();
      } else if (builds) {
        this.alertBuildStatus(builds);
      }
    });
  }

  render() {
    const parameters = this.state.parameters;
    if (Object.keys(parameters).length === 0) {
      return NewmanText('Preparing parameters, Your Majesty.', giantJenkins, 'giant-jenkins');
    }

    return (
      <form onSubmit={this.handleSubmit}>
        {BUILD_TYPE_SORT_MAP.map(type => this.getParamsByType(type, parameters[type]))}
        <input className="button submit-input send-builds" type="submit" value="buildy build" />
      </form>
    );
  }
}


// -----------------------------------------------------------------------------
// exports
// -----------------------------------------------------------------------------
export default BuildForm;
