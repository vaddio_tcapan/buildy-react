import React from 'react';
import newman from '../../assets/newman.gif';
import SpeechBubbleSvg from '../../assets/speech-bubble-svg';


export default (message, image, className) => (
  <div className={`newman-container ${className}`}>
    <img alt="" src={image || newman} className="ah-ah-ah" />
    <SpeechBubbleSvg />
    <span className="newman-text">{message}</span>
  </div>
);
