import React, { Component } from 'react';

class TextInput extends Component {
  static getDerivedStateFromProps(nextProps, prevState) {
    const value = nextProps.value;

    if (value && value !== prevState.value) {
      return {
        value
      };
    }
    return null;
  }

  state = {
    value: this.props.value || ''
  }

  onChange = (event) => {
    const value = event.target.value;
    this.setState({ value });
    this.props.onInputChange(value, this.props.name);
  }

  render() {
    return (
      <input
        className={`text-input ${this.props.className}`}
        type="search"
        placeholder={this.props.placeholder}
        value={this.state.value}
        onChange={this.onChange}
        title={this.props.title}
        name={this.props.name}
        autoComplete={this.props.autoComplete}
      />
    );
  }
}

export default TextInput;
