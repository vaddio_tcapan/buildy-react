// -----------------------------------------------------------------------------
// Imports
// -----------------------------------------------------------------------------
import React, { Component } from 'react';
import axios from 'axios';
import { Route, Link, Switch } from 'react-router-dom';
import PNotify from 'pnotify/dist/es/PNotifyCompat';
import 'pnotify/dist/PNotifyBrightTheme.css';
import 'nonblockjs/NonBlock.es5';
import './styles.css';

import auth from '../../Auth/Auth';
import { TYPES, TYPES_MAP, USER_DATA_KEY } from '../../constants';
import UserContext from '../../contexts/user-context';
import Buildy from '../Buildy';
import Header from '../Header';
import socket from '../../socket-client';
// -----------------------------------------------------------------------------
// Constants
// -----------------------------------------------------------------------------


// -----------------------------------------------------------------------------
// Component
// -----------------------------------------------------------------------------
class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      authLock: null,
      loggedIn: auth.isLoggedIn(),
      user: {
        user_data: JSON.parse(localStorage.getItem(USER_DATA_KEY)) || null,
        updateUserData: this.updateUserData
      },
      jenkins_down_pnotify: ''
    };
  }

  componentWillMount() {
    const authLock = auth.initAuthLock();

    const notification = new PNotify({
      type: 'error',
      title: 'Jenkins Connection Error',
      autoDisplay: false,
      text: `Buildy might be having trouble communicating with Jenkins.\
        If Jenkins is up and running normally, contact buildy's pappy.\
      `,
      hide: false,
      buttons: {
        closer: false,
        sticker: false
      },
      addClass: 'nonblock'
    });

    this.setState({
      authLock,
      jenkins_down_pnotify: notification
    });

    authLock.on('authenticated', (authResult) => {
      authLock.getProfile(authResult.accessToken, (err, profile) => {
        if (err) {
          console.error(err);
          authLock.show();
          return;
        }

        auth.setAccessToken(authResult.accessToken);
        auth.setProfile(JSON.stringify(profile));
        auth.dbSaveOrUpdateUser(profile).then((response) => {
          const loggedIn = auth.isLoggedIn();
          this.setState({ loggedIn });

          // Use getUserData for now, to allow for testing jenkins api info.
          // Server functionality could be cleaned up to make this extra get unecessary.
          // this.updateUserData(response.data);
          this.getUserData();
        });
      });
    });
  }

  componentDidMount() {
    socket.open();
    this.getUserData();

    socket.on('jenkins-status', (status) => {
      if (!status) {
        this.state.jenkins_down_pnotify.open();
      } else if (status && this.state.jenkins_down_pnotify) {
        this.state.jenkins_down_pnotify.close();
      }
    });
  }

  componentWillUnmount() {
    this.state.jenkins_down_pnotify.destroy();

    socket.off('jenkins-status');
    socket.close();
  }

  onLogout = () => {
    const user = { ...this.state.user };
    user.user_data = null;

    auth.logout();

    this.setState({
      user,
      loggedIn: false
    });
  }

  getUserData = () => {
    const user_id = auth.getUserID();
    if (!user_id) return;
    const state = { ...this.state };

    axios.get(`/api/user/data/${user_id}`).then((res) => {
      state.user.user_data = res.data;

      if (!res.data) {
        auth.logout();
        state.user.user_data = null;
        state.loggedIn = false;
      }

      this.setState(state);
      localStorage.setItem(USER_DATA_KEY, JSON.stringify(state.user.user_data));
    });
  }

  updateUserData = (data) => {
    const user = { ...this.state.user };
    const loggedIn = auth.isLoggedIn();
    user.user_data = data;
    this.setState({
      user,
      loggedIn
    });
  }

  render() {
    const loggedIn = this.state.loggedIn;
    const authLock = this.state.authLock;
    const user = this.state.user;

    const path = this.props.location.pathname.replace('/', '');
    const selected_type = TYPES.includes(path) ? path : 'all';

    return (
      <UserContext.Provider value={user}>
        <div className="wrapper">
          <Header
            authLock={authLock}
            loggedIn={loggedIn}
            appLogout={this.onLogout}
          />
          <ul className="type-filters">
            {TYPES.map((type) => {
              const active_class = type === selected_type ? 'active' : '';
              return (
                <li className="type-filter" key={type}>
                  <Link className={active_class} to={`/${type}`}>{TYPES_MAP[type]}</Link>
                </li>
              );
            })}
          </ul>
          <Switch>
            <Route path="/:type" component={Buildy} />
            <Route path="/" component={Buildy} />
          </Switch>
        </div>
      </UserContext.Provider>
    );
  }
}


// -----------------------------------------------------------------------------
// Exports
// -----------------------------------------------------------------------------
export default App;
