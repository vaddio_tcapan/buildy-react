/**
 * IMPORTS
 */
import React, { Component } from 'react';
import './styles.css';
import auth from '../../Auth/Auth';
import BowtieSvg from '../../assets/bowtie-svg';

/**
 * VIEWS
 */
class Header extends Component {
  constructor(props) {
    super(props);
    this.onLogout = this.onLogout.bind(this);
    this.onLogin = this.onLogin.bind(this);
  }

  onLogout() {
    this.props.appLogout();
  }

  onLogin() {
    auth.login(this.props.authLock);
  }

  render() {
    let jenkins_version = '';
    if (window.location.port === '8654') {
      jenkins_version = 'DEV BUILDY';
    }

    return (
      <header className="header">
        <div className="title"><BowtieSvg />buildy
          <span className="jenkins-version">{jenkins_version}</span>
        </div>
        <nav className="navbar navbar-default">
          <ul className="nav navbar-nav navbar-right">
            <li>
              {
              (this.props.loggedIn)
              ? (<button className="auth" onClick={this.onLogout}>Logout</button>)
              : (<button className="auth" onClick={this.onLogin}>Login</button>)
              }
            </li>
          </ul>
        </nav>
      </header>
    );
  }
}


/**
 * EXPORTS
 */
export default Header;
