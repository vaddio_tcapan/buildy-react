import React from 'react';

export default React.createContext({
  user_data: null,
  updateUserData: null
});
