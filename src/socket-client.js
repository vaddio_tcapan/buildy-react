import io from 'socket.io-client';
import { VERSION } from '../app/server-constants';
import { USER_DATA_KEY } from './constants';


const user_data = JSON.parse(localStorage.getItem(USER_DATA_KEY));

const user = user_data ? user_data.name || user_data.email : 'Unknown';

// Should use current url if nothing passed. Otherwise use constants.
// False autoconnect for now and call #open in first component that makes use of it
// at this time.
const socket = io({ autoConnect: false, query: `user=${user}` });


// Socket logic that's not specific to any component.
socket.on('version', (version) => {
  if (version === VERSION) {
    console.log(`Client and server versions match (${version})`);
  } else {
    console.log(`Client version ${version} does not match server version ${VERSION}. Reloading.`);
    window.location.reload(true);
  }
});

socket.on('force-reload', () => window.location.reload(true));

export default socket;
