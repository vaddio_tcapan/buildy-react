import React from 'react';
import { Route } from 'react-router';

import App from './components/App';

const Routes = () => (
  <div>
    <Route component={App} />
    <Route status={404} path="*" />
  </div>
);

export default Routes;
