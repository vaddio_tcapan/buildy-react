const body = document.body;
const ANIMATION_SYNC_CSS_CLASS = 'animation-sync';

function addClass() {
  body.classList.add(ANIMATION_SYNC_CSS_CLASS);
}

function removeClass() {
  body.classList.remove(ANIMATION_SYNC_CSS_CLASS);
  window.requestAnimationFrame(addClass);
}

function syncCssAnimations() {
  window.requestAnimationFrame(removeClass);
}

function toTitleCase (s) {
  const RE_TITLECASE = /\w\S*/g;
  return s.toString().replace(
    RE_TITLECASE,
    word => word.charAt(0).toUpperCase() + word.substr(1)
  );
}

function paramToTitle(name) {
  return toTitleCase(name.replace(/_/g, ' '));
}

function machineToTitle(machine) {
  if (!machine) return '';
  return toTitleCase(machine.replace('vaddio-', ''));
}

function getDate(timestamp, dayOnly) {
  const date = new Date(timestamp);
  let hour = date.getHours();
  let minutes = date.getMinutes();
  let meridiem = ' AM';

  minutes = (minutes < 10) ? `0${minutes}` : minutes;

  if (hour > 12) {
    hour -= 12;
    meridiem = ' PM';
  } else if (hour === 12) {
    meridiem = ' PM';
  }

  const day = `${(date.getMonth() + 1)}/${date.getDate()}/${date.getFullYear().toString().slice(2)}`;

  if (dayOnly) return day;

  return `
    ${day}
    ${hour}:${minutes}
    ${meridiem}
  `;
}

// -----------------------------------------------------------------------------
// Exports
// -----------------------------------------------------------------------------
export default {
  toTitleCase,
  paramToTitle,
  machineToTitle,
  syncCssAnimations,
  getDate
};
