Another potential issue: stale build data when window left idle for a while? I've
see very few builds show up at times and a refresh seemed to get more...
++ Saw this again. V few builds present. Need to check logic of updating all builds.
Is there a case where if a single jenkins build type request fails to get all the builds
and others succeed and then we replace all with the partial data? if that's the case
and I can't find why we get so many request failures these days (new jenkins) I might
need some kind of hacky check on how many builds come in and only replace all if the numbers
aren't way different. Or some kind of delay on when buildy gets rid of them that spans
at least a couple 'get all builds'


david c issue (see slack) kick off test build only from 5 diff commits. see what
client sends and what server sends to jenkins. probaly worth spinning up a dev server

sending builds dialog not updating if page has been open for too long or is it because
the response is delayed. looks like jenkins is sometimes slow to respond. might be
linked to buildy server getting all those error and needing to be restarted......

assess error handling again. Still occasionally leaving error message up after jenkins
is back up and running properly. probably ping less frequently? the ping isn't
logging to console so why is onJenkinsHttpError being called so frequently (just
the build polling etc)

add prapti update task in somehow

add cancel build option?

validate that specified branch/commit exists in repo

streamline the #flattenBuilds this could probably use some recursion instead of checking for specific pieces of data.

https://github.com/romainberger/react-portal-tooltip/issues
  - if build is in progress, mouseover tooltip, move mouse outside of tooltip/parent to remove from dom.
  - do the above twice on the in progress build or any amount of times elsewhere and then once on in progress build.
  - ^ not totally accurate, right now top two builds (one in prog, one not) both cause this

pnofify compat error? i think it's just after development/hmr/nodemon reloading or sumtin
  Uncaught TypeError: Cannot convert undefined or null to object
    at Function.assign (<anonymous>)
    at PNotifyCompat.get (_iter-define.js:70)
    at Function.PNotify.positionAll (_iter-create.js:14)
    at setTimeout (_iter-create.js:14)

Looks like jenkins err is getting logged 6 times (instead of once?) every 2s when it can't actually connect

Why aren't builds staying in DB when jenkins is down. this was remotely with lost VPN so it might not always be the case

With Jenkins Connection Errors, the db interaction seems to get a little wonky and throws some errors.
I think I've seen the front end without a lot of the build data in this situation. Worth exploring since
these error seem to get thrown most days.

ERROR stuff:
Jenkins Connection ERROR:  socket hang up
Jenkins Connection ERROR:  read ECONNRESET
Jenkins Connection ERROR:  connect ETIMEDOUT 10.1.1.53:80


command line integration: kicking off most useful, searching next.


add nodemailer to server for when jenkins is down or when builds fail to be created.


Populate commit with branch most recently pushed to bitbucket? awesomeplete?
Will likely need to use more thorough auth for this since we'd likely need to be
authorized for every ping to bitbucket


validate build form submissions and jenkins user data form submissions before sending to server


store previous n searches and/or build forms in localstorage and provide arrows to move between them.


search builds with regex


websocket initial ALL build data is getting to client but isn't shown in chrome debugger?
  - it might be anything that's being sent in the on 'connection' callbacks, that isn't
    showing up in debugger


CURRENT PRIORITY TASKS:
  - Clean up dist/prod folder, prob just rimraf folder with 'prod' task. It seems
    like Parcel maybe handles cleaning up the root dist dir.
  - nginx redirect?
  - build tooltip details?
  - build status alerts

  - verify/fix auth setup - seems acceptable at the moment

UNNECESSARY/OVERKILL LEARNING OPPORTUNITIES:
  - Server side render the build list
  - css modules, styled components
  - Relay, apollo, graphql, redux...
