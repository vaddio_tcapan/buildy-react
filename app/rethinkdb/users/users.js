const crypto = require('crypto');
const _ = require('underscore');
const request = require('request');
const thinky = require('../config.js');
const constants = require('../../server-constants');

const algorithm = 'aes-256-ctr';
const cryptoPassword = 'superSecretPasswordStuff1212';
const type = thinky.type;


// -----------------------------------------------------------------------------
// Constants
// -----------------------------------------------------------------------------
const PICK_MODEL_KEYS = [
  'user_id',
  'email',
  'name',
  'nickname',
  'picture',
  'has_valid_jenkins_auth'
];

// -----------------------------------------------------------------------------
// Schema
// -----------------------------------------------------------------------------
const User = thinky.createModel('Users', {
  user_id: type.string(),
  email: type.string(),
  name: type.string(),
  nickname: type.string(),
  picture: type.string(),
  jenkins_username: type.string(),
  jenkins_api_token: type.string(),
  has_valid_jenkins_auth: type.boolean()
}, {
  pk: 'user_id'
});


// -----------------------------------------------------------------------------
// Local
// -----------------------------------------------------------------------------
function findUser(user_id) {
  return User.get(user_id).run();
}

function encrypt(text, next) {
  // TODO: createCipher is deprecated. Use createCipheriv. Maybe not an easy swap.
  const cipher = crypto.createCipher(algorithm, cryptoPassword);
  let crypted = cipher.update(text, 'utf8', 'hex');
  crypted += cipher.final('hex');
  next(crypted);
}

function decrypt(text, next) {
  const decipher = crypto.createDecipher(algorithm, cryptoPassword);
  let dec = decipher.update(text, 'hex', 'utf8');
  dec += decipher.final('utf8');
  next(dec);
}

async function hasValidJenkinsAuth(user) {
  // TODO: this doesn't need to be an async function anymore.
  const auth_test = await new Promise((resolve) => {
    console.log('hasValidJenkinsAuth');

    request
      .get(constants.BASE_JENKINS_URL, {
        auth: {
          user: user.jenkins_username,
          pass: user.jenkins_api_token
        }
      })
      .on('response', (response) => {
        resolve(response.statusCode !== 401);
      })
      .on('error', (error) => {
        console.error(error);
      });
  });

  return auth_test;
}


// -----------------------------------------------------------------------------
// User Hooks
// -----------------------------------------------------------------------------
User.pre('save', function(next) {
  if (!this.jenkins_api_token) {
    next();
  } else {
    encrypt(this.jenkins_api_token, (newToken) => {
      hasValidJenkinsAuth(this).then((response) => {
        this.has_valid_jenkins_auth = response;
        this.jenkins_api_token = newToken;
        next();
      });
    });
  }
});


function saveOrUpdateUser(user_data, next) {
  return User.save(user_data, { conflict: 'update' })
    .then((result) => {
      const user = _.pick(result, PICK_MODEL_KEYS);
      if (_.isFunction(next)) next(user);
    })
    .error((err) => {
      console.error('UserController error:', err);
    });
}

function getUserById(user_id, next) {
  findUser(user_id)
    .then((result) => {
      const user = _.pick(result, PICK_MODEL_KEYS);
      const test_jenkins_user = _.omit(result, PICK_MODEL_KEYS);

      if (test_jenkins_user.jenkins_api_token && test_jenkins_user.jenkins_username) {
        decrypt(test_jenkins_user.jenkins_api_token, (realToken) => {
          test_jenkins_user.jenkins_api_token = realToken;

          hasValidJenkinsAuth(test_jenkins_user).then((response) => {
            if (user.has_valid_jenkins_auth !== response) {
              user.has_valid_jenkins_auth = response;
              saveOrUpdateUser(user);
            }
            next(user);
          });
        });
      } else {
        next(user);
      }
    })
    .error((err) => {
      next(null);
      console.error(err);
    });
}

function getUserJenkinsAuth(user_id, next) {
  findUser(user_id)
    .then((result) => {
      if (!result) next(false, null);

      const user = _.pick(result, PICK_MODEL_KEYS);
      const jenkins_auth_data = _.omit(result, PICK_MODEL_KEYS);

      if (jenkins_auth_data.jenkins_api_token && jenkins_auth_data.jenkins_username) {
        decrypt(jenkins_auth_data.jenkins_api_token, (realToken) => {
          jenkins_auth_data.jenkins_api_token = realToken;

          hasValidJenkinsAuth(jenkins_auth_data).then((response) => {
            if (user.has_valid_jenkins_auth !== response) {
              user.has_valid_jenkins_auth = response;
              saveOrUpdateUser(user);
            }

            if (!user.has_valid_jenkins_auth) {
              next(false, user);
            } else {
              // Only return jenkins auth if it's there and valid
              next(true, jenkins_auth_data);
            }
          });
        });
      } else {
        if (user.has_valid_jenkins_auth) {
          user.has_valid_jenkins_auth = false;
          saveOrUpdateUser(user);
        }
        next(false, user);
      }
    }).error((err) => {
      next(false, null);
      console.error(err);
    });
}

// -----------------------------------------------------------------------------
// Exports
// -----------------------------------------------------------------------------
module.exports = {
  saveOrUpdateUser,
  getUserById,
  getUserJenkinsAuth
};
