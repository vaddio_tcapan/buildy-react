/* eslint max-len: ["error", { "code": 400 }] */

const request = require('request');
const helpers = require('../../helpers.js');
const thinky = require('../config.js');
const onJenkinsHttpError = require('../../jenkins-watch').onJenkinsHttpError;
const constants = require('../../server-constants');

const type = thinky.type;

let base_jenkins_url = constants.BASE_JENKINS_URL;

if (process.env.JENKINS_ENV === 'newish') {
  base_jenkins_url = constants.NEWISH_BASE_JENKINS_URL;
}

const GET_ARM_CONTAINERIZED_BUILDS = `${base_jenkins_url}/job/yocto-containerized-build/api/json?tree=allBuilds[description,id,building,number,url,result,keepLog,timestamp,actions[lastBuiltRevision[SHA1],parameters[name,value],causes[userId,userName]],artifacts[displayPath,fileName,relativePath]]{0,`;

const GET_ARM_PARAMETERIZED_BUILDS = `${base_jenkins_url}/job/yocto-parameterized-build/api/json?tree=allBuilds[description,id,building,number,url,result,keepLog,timestamp,actions[lastBuiltRevision[SHA1],parameters[name,value],causes[userId,userName]],artifacts[displayPath,fileName,relativePath]]{0,`;


const GET_ARK_PARAMETERIZED_BUILDS = `${base_jenkins_url}/job/yocto-containerized-build-ark-builder/api/json?tree=allBuilds[description,id,building,number,url,result,keepLog,timestamp,actions[lastBuiltRevision[SHA1],parameters[name,value],causes[userId,userName]],artifacts[displayPath,fileName,relativePath]]{0,`;

const GET_WEB_TEST_BUILDS = `${base_jenkins_url}/view/Tests/job/test-web-unit/api/json?tree=builds[description,id,building,number,url,result,keepLog,timestamp,actions[lastBuiltRevision[SHA1],parameters[name,value],causes[userId,userName]]]{0,`;

// const GET_PYTHON_TEST_BUILDS = `${base_jenkins_url}/view/Tests/job/test-python-unit/api/json?tree=builds[description,id,building,number,url,result,keepLog,timestamp,actions[lastBuiltRevision[SHA1],parameters[name,value],causes[userId,userName]]]{0,`;
const GET_NEW_PYTHON_3_TOX_TEST_BUILDS = `${base_jenkins_url}/view/Tests/job/test-python3-tox/api/json?tree=builds[description,id,building,number,url,result,keepLog,timestamp,actions[lastBuiltRevision[SHA1],parameters[name,value],causes[userId,userName]]]{0,`;

const GET_REST_TEST_BUILDS = `${base_jenkins_url}/view/Tests/job/REST%20Regression%20Test/api/json?tree=builds[description,id,building,number,url,result,keepLog,timestamp,actions[lastBuiltRevision[SHA1],parameters[name,value],causes[userId,userName]]]{0,`;


// -----------------------------------------------------------------------------
// Schema
// -----------------------------------------------------------------------------
/*
  Example data from Jenkins:

  COMMIT: "9521be03235dc444b10d9526253d70aa71c7ebb7"
  FILESYSTEM_IMAGE_VERSION:"debug"
  MACHINE:"vaddio-remus"
  MW_SERVICE_SELF_START:false
  SHA:"9521be03235dc444b10d9526253d70aa71c7ebb7"
  VAD_PRODUCT_VERSION:"VNG-8676: Log I2c write errors"
  VNG_REPO:"git@bitbucket.org:vaddio/vaddio_mcouture-vng.git"
  artifacts:(4) [{…}, {…}, {…}, {…}]
  building:false
  description:null
  id:"2018-02-20_16-47-47"
  number:20757
  result:"SUCCESS"
  timestamp:1519166867758
  url:"http://jenkins.vaddio.com/jenkins/job/yocto-containerized-build/20757/"
  userId:"michaelc"
  userName:"Michael Couture"
 */

const Build = thinky.createModel('Builds', {
  // ARM builds only, also storing ARK: PRODUCT here and test type, for front-end simplicity.
  MACHINE: type.string().allowNull(true),

  // null for tests
  VAD_PRODUCT_VERSION: type.string().allowNull(true),

  // Jenkins keep forever - might be worth flagging for users to see.
  keepLog: type.boolean(),

  FILESYSTEM_IMAGE_VERSION: type.string().allowNull(true),
  MW_SERVICE_SELF_START: type.boolean().allowNull(true),
  SHA: type.string(),
  COMMIT: type.string(),
  VNG_REPO: type.string(),
  building: type.boolean(),
  description: type.string().allowNull(true),
  id: type.string(),
  result: type.string(),
  timestamp: type.number(),
  url: type.string(),
  userId: type.string().default('Nightly'),
  userName: type.string().default('Knight, Lee'),
  type: type.string(),

  // REST TEST ONLY
  DeviceType: type.string().allowNull(true),
  DeviceIP: type.string().allowNull(true),
  DeviceUser: type.string().allowNull(true)

  // artifacts: type.array(),
  // createdAt: type.date().default(thinky.r.now())
}, {
  pk: 'id'
});


// -----------------------------------------------------------------------------
// Controller
// -----------------------------------------------------------------------------
function getBuilds(query, count) {
  return new Promise((resolve) => {
    request
      .get(query + count)
      .auth(constants.JENKINS_USERNAME, constants.JENKINS_API_TOKEN)
      .on('response', (response) => {
        if (response.statusCode > 299) {
          onJenkinsHttpError(`Jenkins status code ERROR: ${response.statusCode}`);
          // console.log(response.request.href);
          resolve('[]');
          return;
        }

        let builds = '';
        response.on('data', (chunk) => {
          builds += chunk;
        });
        response.on('end', () => {
          resolve(builds);
        });
      })
      .on('error', (error) => {
        onJenkinsHttpError(error);
        resolve('[]');
      });
  });
}

function getSomeBuilds(count) {
  // Gets count or all builds.
  const current_count = count ? `${count}}` : '}';

  // Only get a couple tests or all
  const test_count = count ? '2}' : '}';

  const arm_containerized_builds = getBuilds(GET_ARM_CONTAINERIZED_BUILDS, current_count);
  const arm_parameterized_builds = getBuilds(GET_ARM_PARAMETERIZED_BUILDS, current_count);
  const ark_parameterized_builds = getBuilds(GET_ARK_PARAMETERIZED_BUILDS, current_count);
  const test_web_builds = getBuilds(GET_WEB_TEST_BUILDS, test_count);
  const test_python_builds = getBuilds(GET_NEW_PYTHON_3_TOX_TEST_BUILDS, test_count);
  const test_rest_builds = getBuilds(GET_REST_TEST_BUILDS, test_count);

  // Get all build types here and pass in a specified order for helpers#flattenBuilds to iterate and add a type
  return Promise.all([
    arm_containerized_builds,
    arm_parameterized_builds,
    ark_parameterized_builds,
    test_web_builds,
    test_python_builds,
    test_rest_builds
  ]).then(builds => helpers.flattenBuilds(builds));
}

function saveOrUpdateBuilds(builds) {
  // This needs to reset the db when getting/saving all builds to remove builds that have
  // been removed
  return Build.save(builds, { conflict: 'update' })
    .then((result) => {
      // console.log('SAVED: ', result.length);
      return builds;
    })
    .error((err) => {
      console.error('buildController error:', err);
      return builds;
    });
}

function getAndSaveBuilds(count) {
  console.log('POLLING: getAndSaveBuilds');
  return new Promise((resolve) => {
    getSomeBuilds(count).then((builds) => {
      saveOrUpdateBuilds(builds).finally(() => {
        resolve(builds);
      });
    });
  });
}

function getDbBuilds() {
  return Build.run();
}


// -----------------------------------------------------------------------------
// Build fetching
// -----------------------------------------------------------------------------
let getBuildsInterval;
let stopGetBuildsTimeout;

function clearGetBuildsInterval() {
  if (getBuildsInterval) {
    clearInterval(getBuildsInterval);
    getBuildsInterval = null;
  }
}

function updateStopGetBuildsTimeout() {
  console.log('updateStopGetBuildsTimeout');

  if (stopGetBuildsTimeout) {
    clearTimeout(stopGetBuildsTimeout);
  }

  stopGetBuildsTimeout = setTimeout(() => {
    clearGetBuildsInterval();
    stopGetBuildsTimeout = null;
  }, 1 * 60 * 1000);
}

// TODO: call this when builds are kicked off and from build-change-feed when wanted
function updateGetBuildsInterval() {
  updateStopGetBuildsTimeout();

  if (getBuildsInterval) return;

  getBuildsInterval = setInterval(() => {
    console.log('POLLING: 20 BUILDS');
    getAndSaveBuilds(20);
  }, 10 * 1000);
}

function getAllBuilds() {
  console.log('POLLING: ALL BUILDS');
  const db_builds = Build.run();
  const jenkins_builds = getAndSaveBuilds();

  Promise.all([db_builds, jenkins_builds]).then(([dbBuilds, jenkinsBuilds]) => {
    const jenkins_build_ids = jenkinsBuilds.map(build => build.id);

    dbBuilds.forEach((build) => {
      if (jenkins_build_ids.includes(build.id)) return;
      build.delete();
    });
  });

  setTimeout(() => {
    getAllBuilds();
  }, 2 * 60 * 1000);
}

getAllBuilds();


// -----------------------------------------------------------------------------
// Exports
// -----------------------------------------------------------------------------
module.exports = {
  getDbBuilds,
  Build,
  updateGetBuildsInterval
};
