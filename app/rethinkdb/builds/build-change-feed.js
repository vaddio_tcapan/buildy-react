const builds = require('./builds');

module.exports = (io) => {
  builds.Build.changes().then((feed) => {
    feed.each((error, build) => {
      if (error) {
        console.log(error);
      }
      if (build.isSaved() === false) {
        io.sockets.emit('build', 'DELETE', build);
      } else if (build.getOldValue() == null) {
        io.sockets.emit('build', 'INSERT', build);

        // Start or extend polling for build changes.
        builds.updateGetBuildsInterval();
        console.log('POLLING update: INSERT');
      } else {
        io.sockets.emit('build', 'REPLACE', build);

        if (build.building) {
          builds.updateGetBuildsInterval();
          console.log('POLLING update: REPLACE, build.building');
        }
      }
    });
  });
};
