
const constants = require('../server-constants');


// -----------------------------------------------------------------------------
// Constants
// -----------------------------------------------------------------------------
let port = 28015;
let db = 'Buildy';

if (process.env.JENKINS_ENV === 'newish') {
  // port = 28016;
  db = 'NewishBuildy';
  console.log('Using NewishBuildy DB table');
}

// Databse setup
const dbHost = (process.env.DB_PORT || 'localhost');
const rethinkdb = {
  host: dbHost,
  port: port,
  authKey: '',
  db: db
};

const thinky = require('thinky')(rethinkdb);

module.exports = thinky;
