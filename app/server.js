const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const http = require('http');
const Bundler = require('parcel-bundler');
const routes = require('./routes');
const constants = require('./server-constants');
const Builds = require('./rethinkdb/builds/builds');
const buildChangeFeed = require('./rethinkdb/builds/build-change-feed');
const buildParametersChangeFeed = require('./build-parameters/build-parameters').changeFeed;
const jenkinsStatusChangeFeed = require('./jenkins-watch').jenkinsStatusChangeFeed;

const app = express();
const relative_path = path.dirname(`${__dirname}`);
const bundler = new Bundler('./src/index.html');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

routes(app);

if (process.env.NODE_ENV === 'prod') {
  app.use(express.static(relative_path));
  app.use((req, res) => { res.sendFile(path.join(relative_path, 'index.html')); });
  console.log('--------------------------PRODUCTION SERVER RUNNING');
} else {
  app.use(bundler.middleware());
  console.log('--------------------------DEVELOPMENT SERVER RUNNING');
}

const server = http.Server(app);

// socketConnect(server);
const io = require('socket.io')(server);

io.on('connection', (socket) => {
  // console.log(`SocketIO client connected :: ${socket.handshake.query.user}`);

  socket.emit('version', constants.VERSION);

  Builds.getDbBuilds().then((builds) => {
    socket.emit('build', 'ALL', builds);
  });
});

// Init websocket feeds.
buildChangeFeed(io);
buildParametersChangeFeed(io);
jenkinsStatusChangeFeed(io);

module.exports = server;
