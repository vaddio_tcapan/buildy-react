const VERSION = '1.3.0';

const DEFAULT_PORT = 1234;


const BASE_JENKINS_URL = 'http://10.30.12.53/jenkins';
const NEWISH_BASE_JENKINS_URL = 'http://jenkins.vaddio.com/jenkins';
// const NEWISH_BASE_JENKINS_URL = 'http://jenkinsprodmt01.vaddio.com/jenkins';

const JENKINS_USERNAME = 'capant01';
const JENKINS_API_TOKEN = '2f29f846dc8ea3a22a1e35ef44a4eb91';

const SPLIT_PARAMS = [
  'MACHINE',
  'PRODUCT'
];

const DEFAULT_BUILD_KEYS = {
  arm: ['VNG_REPO', 'COMMIT', 'MACHINE', 'FILESYSTEM_IMAGE_VERSION', 'VAD_PRODUCT_VERSION', 'MW_SERVICE_SELF_START', 'EXPORT_BUILD_CACHES'],
  ark: ['VNG_REPO', 'COMMIT', 'PRODUCT', 'FILESYSTEM_IMAGE_VERSION', 'VAD_PRODUCT_VERSION', 'MW_SERVICE_SELF_START', 'EXPORT_BUILD_CACHES'],
  web: ['VNG_REPO', 'COMMIT'],

  // Old python test params
  // python: ['PYTHON_PACKAGE_NAME', 'SUB_PACKAGE_PATH', 'VNG_REPO', 'COMMIT', 'PYLINT_CFG'],

  // New python3 tox test params
  python: ['VNG_REPO', 'COMMIT', 'TOX_ARGS'],

  rest: ['DeviceType', 'DeviceIP', 'DeviceUser', 'Password', 'VNG_REPO', 'COMMIT']
};

module.exports = {
  VERSION,
  DEFAULT_PORT,
  JENKINS_USERNAME,
  JENKINS_API_TOKEN,
  SPLIT_PARAMS,
  BASE_JENKINS_URL,
  NEWISH_BASE_JENKINS_URL,
  DEFAULT_BUILD_KEYS
};
