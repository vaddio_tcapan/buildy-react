// -----------------------------------------------------------------------------
// Imports
// -----------------------------------------------------------------------------
const events = require('events');
const request = require('request');

const constants = require('./server-constants');


// -----------------------------------------------------------------------------
// Constants
// -----------------------------------------------------------------------------
let base_jenkins_url = constants.BASE_JENKINS_URL;

if (process.env.JENKINS_ENV === 'newish') {
  base_jenkins_url = constants.NEWISH_BASE_JENKINS_URL;
}

let JENKY_STATUS = true;
const eventEmitter = new events.EventEmitter();
let JENKINS_PING_INTERVAL;

// -----------------------------------------------------------------------------
// Jenkins error handling
// -----------------------------------------------------------------------------
function pingJenkins() {
  // request.get(base_jenkins_url, {
  //   auth: {
  //     user: constants.JENKINS_USERNAME,
  //     pass: constants.JENKINS_API_TOKEN
  //   }
  // }, (err, res) => {
  //   const jenky_down = err || res.statusCode > 299;
  //   // if Jenkins is down and he was up
  //   if (jenky_down && JENKY_STATUS) {
  //     JENKY_STATUS = false;
  //     eventEmitter.emit('jenkins-status', JENKY_STATUS);
  //   // if Jenkins is up and he was down
  //   } else if (!jenky_down && !JENKY_STATUS) {
  //     JENKY_STATUS = true;
  //     eventEmitter.emit('jenkins-status', JENKY_STATUS, true);

  //     if (JENKINS_PING_INTERVAL) {
  //       clearInterval(JENKINS_PING_INTERVAL);
  //       JENKINS_PING_INTERVAL = null;
  //     }
  //   // if he's up now and we have an interval, clear it by default
  //   } else if (!jenky_down && JENKINS_PING_INTERVAL) {
  //     clearInterval(JENKINS_PING_INTERVAL);
  //     JENKINS_PING_INTERVAL = null;
  //   }
  // });
}

function initJenkinsPing() {
  // JENKINS_PING_INTERVAL = setInterval(() => {
  //   pingJenkins();
  // }, 2 * 1000);
}

function onJenkinsHttpError(error) {
  // if (error && error.message) {
  //   console.error('Jenkins Connection ERROR: ', error.message);
  // } else if (error) {
  //   console.error('Jenkins Connection ERROR: ', error);
  // }

  // if (JENKY_STATUS && !JENKINS_PING_INTERVAL) {
  //   initJenkinsPing();
  // }

  // if (JENKY_STATUS) {
  //   JENKY_STATUS = false;
  //   eventEmitter.emit('jenkins-status', JENKY_STATUS);
  //   initJenkinsPing();
  // }
}

function jenkinsStatusChangeFeed(io) {
  // on reconnection: emit parameters and builds etc?
  // could just force reloads on front
  // io.on('connection', (socket) => {
  //   socket.emit('jenkins-status', JENKY_STATUS);
  // });
  // eventEmitter.on('jenkins-status', (status, reload) => {
  //   io.sockets.emit('jenkins-status', status);

  //   if (reload) {
  //     io.sockets.emit('force-reload');
  //   }
  // });
}


// -----------------------------------------------------------------------------
// Exports
// -----------------------------------------------------------------------------
module.exports = {
  jenkinsStatusChangeFeed,
  onJenkinsHttpError
};
