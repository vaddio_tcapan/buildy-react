/* eslint max-len: ["error", { "code": 300 }] */

const request = require('request');
const _ = require('underscore');
const UserController = require('./rethinkdb/users/users.js');
const helpers = require('./helpers.js');
const constants = require('./server-constants');
const build_keys = require('./build-parameters/build-parameters').build_keys;
const onJenkinsHttpError = require('./jenkins-watch').onJenkinsHttpError;
const builds = require('./rethinkdb/builds/builds.js');

// -----------------------------------------------------------------------------
// Constants
// -----------------------------------------------------------------------------
let base_jenkins_url = constants.BASE_JENKINS_URL;

if (process.env.JENKINS_ENV === 'newish') {
  base_jenkins_url = constants.NEWISH_BASE_JENKINS_URL;
}


const ARM_SUBMIT_BUILD_QUERY = `${base_jenkins_url}/job/yocto-containerized-build/buildWithParameters?`;

const ARK_SUBMIT_BUILD_QUERY = `${base_jenkins_url}/job/yocto-containerized-build-ark-builder/buildWithParameters?`;

const WEB_SUBMIT_TEST_BUILD_QUERY = `${base_jenkins_url}/view/Tests/job/test-web-unit/buildWithParameters?`;

// const PYTHON_SUBMIT_TEST_BUILD_QUERY = `${base_jenkins_url}/view/Tests/job/test-python-unit/buildWithParameters?`;

const NEW_PYTHON_3_TOX_SUBMIT_TEST_BUILD_QUERY = `${base_jenkins_url}/view/Tests/job/test-python3-tox/buildWithParameters?`;

const REST_SUBMIT_TEST_BUILD_QUERY = `${base_jenkins_url}/view/Tests/job/REST%20Regression%20Test/buildWithParameters?`;

const BUILD_TYPE_URL_MAP = {
  arm: ARM_SUBMIT_BUILD_QUERY,
  ark: ARK_SUBMIT_BUILD_QUERY,
  web: WEB_SUBMIT_TEST_BUILD_QUERY,
  python: NEW_PYTHON_3_TOX_SUBMIT_TEST_BUILD_QUERY,
  rest: REST_SUBMIT_TEST_BUILD_QUERY
};


// -----------------------------------------------------------------------------
// Exports (Routes)
// -----------------------------------------------------------------------------
module.exports = function(app) {
  // -----------------------------------------------------------------------------
  // Build Routes
  // -----------------------------------------------------------------------------
  app.post('/api/builds', (req, res) => {
    // Split off 'MACHINE' and 'PRODUCT' for later iteration
    const parameters = helpers.separateMachines(req.body);
    const params = parameters.params;
    const build_urls = [];
    const types_to_build = [];
    const build_statuses = [];
    const arm_builds = parameters.MACHINE.length;
    const ark_builds = parameters.PRODUCT.length;
    let totalBuilds = arm_builds + ark_builds;
    let jenkins_username;
    let jenkins_api_token;

    if (arm_builds) types_to_build.push('arm');
    if (ark_builds) types_to_build.push('ark');

    if (params.WEB_TEST) {
      types_to_build.push('web');
      totalBuilds += 1;
    }

    if (params.PYTHON_TEST) {
      types_to_build.push('python');
      totalBuilds += 1;
    }

    if (params.REST_TEST) {
      types_to_build.push('rest');
      totalBuilds += 1;
    }

    UserController.getUserJenkinsAuth(params.user_id, (success, user) => {
      if (!success) {
        res.send({ user });
        return;
      }

      jenkins_username = user.jenkins_username;
      jenkins_api_token = user.jenkins_api_token;

      types_to_build.forEach((type) => {
        const type_keys = build_keys.keys[type];
        const filtered_params = _.pick(params, type_keys);
        let build_params;
        let stringified_params;
        let url;

        // e.g 'MACHINE' or 'PRODUCT'. These can have multiple so we need to
        // iterate and form multiple requests.
        const split_param = _.first(_.intersection(constants.SPLIT_PARAMS, type_keys));

        if (split_param) {
          parameters[split_param].forEach((val) => {
            build_params = { ...filtered_params };
            build_params[split_param] = val;
            stringified_params = helpers.stringifyParameters(build_params);
            url = `${BUILD_TYPE_URL_MAP[type]}${stringified_params}`;

            build_urls.push(url);
          });
        } else {
          stringified_params = helpers.stringifyParameters(filtered_params);
          url = `${BUILD_TYPE_URL_MAP[type]}${stringified_params}`;
          build_urls.push(url);
        }
      });

      build_urls.forEach((url) => {
        console.log('POLLING: build request: ', url);

        request
          .post(url)
          .auth(jenkins_username, jenkins_api_token)
          .on('response', (response) => {
            if (response.statusCode > 299) {
              onJenkinsHttpError(`Jenkins status code ERROR: ${response.statusCode}`);
            }

            const status = helpers.parseBuildStatus(response.statusCode, url);
            build_statuses.push(status);
            console.log(`${jenkins_username} :: ${status.build} :: ${status.created} :: \n${url}\n`);

            if (build_statuses.length === totalBuilds) {
              res.send({ builds: build_statuses });
            }
          })
          .on('error', (error) => {
            const status = helpers.parseBuildStatus(500, url);
            build_statuses.push(status);
            console.log(`${jenkins_username} :: ${status.build} :: ${status.created}`);

            if (build_statuses.length === totalBuilds) {
              res.send({ builds: build_statuses });
            }

            onJenkinsHttpError(error);
          });
      });

      // Start or continue polling for build changes.
      builds.updateGetBuildsInterval();
    });
  });


  // -----------------------------------------------------------------------------
  // User Routes
  // -----------------------------------------------------------------------------
  app.post('/api/user/login', (req, res) => {
    const user_data = req.body;
    UserController.saveOrUpdateUser(user_data, (userData) => {
      res.send(userData);
    });
  });

  app.get('/api/user/data/:user_id', (req, res) => {
    UserController.getUserById(req.params.user_id, (userData) => {
      res.send(userData);
    });
  });
};
