/* eslint max-len: ["error", { "code": 300 }] */

const request = require('request');
const _ = require('underscore');
const helpers = require('../helpers.js');
const constants = require('../server-constants');
const onJenkinsHttpError = require('../jenkins-watch').onJenkinsHttpError;


// -----------------------------------------------------------------------------
// Constants
// -----------------------------------------------------------------------------
let base_jenkins_url = constants.BASE_JENKINS_URL;

if (process.env.JENKINS_ENV === 'newish') {
  base_jenkins_url = constants.NEWISH_BASE_JENKINS_URL;
}


const ARM_CONTAINERIZED_PARAMETERS_QUERY = `${base_jenkins_url}/job/yocto-containerized-build/api/json?tree=actions[parameterDefinitions[defaultParameterValue[value],description,name,type,choices]]`;

const ARK_CONTAINERIZED_PARAMETERS_QUERY = `${base_jenkins_url}/job/yocto-containerized-build-ark-builder/api/json?tree=actions[parameterDefinitions[defaultParameterValue[value],description,name,type,choices]]`;

const WEB_TEST_PARAMETERS_QUERY = `${base_jenkins_url}/view/Tests/job/test-web-unit/api/json?tree=actions[parameterDefinitions[defaultParameterValue[value],description,name,type,choices]]`;

// const PYTHON_TEST_PARAMETERS_QUERY = `${base_jenkins_url}/view/Tests/job/test-python-unit/api/json?tree=actions[parameterDefinitions[defaultParameterValue[value],description,name,type,choices]]`;
const NEW_PYTHON_3_TOX_TEST_PARAMETERS_QUERY = `${base_jenkins_url}/view/Tests/job/test-python3-tox/api/json?tree=actions[parameterDefinitions[defaultParameterValue[value],description,name,type,choices]]`;

const REST_TEST_PARAMETERS_QUERY = `${base_jenkins_url}/view/Tests/job/REST%20Regression%20Test/api/json?tree=actions[parameterDefinitions[defaultParameterValue[value],description,name,type,choices]]`;

const MULTI_SELECT_KEYS = [
  'PRODUCT',
  'MACHINE'
];

/**
 * class to keep track of each build type's parameter keys.
 *
 * For dynamically changing what gets sent to jenkins for each build
 * type, should any new parameters be added.
 */
class BuildKeys {
  constructor() {
    this.keys = constants.DEFAULT_BUILD_KEYS;
  }
}

// Singleton class for export
const build_keys = new BuildKeys();

// -----------------------------------------------------------------------------
// Local
// -----------------------------------------------------------------------------
function getTestBuildBooleanParameter(testType) {
  const test_type = testType === 'rest' ? `${testType} regression` : testType;
  return {
    defaultParameterValue: { value: false },
    description: `Toggle this to include a ${test_type} test build`,
    name: `${testType.toUpperCase()}_TEST`,
    type: 'BooleanParameterDefinition'
  };
}


// -----------------------------------------------------------------------------
// Parameters
// -----------------------------------------------------------------------------
let build_parameters_collection;

function getParameters(query) {
  return new Promise((resolve) => {
    console.log('GET PARAMS');

    request
      .get(query)
      .auth(constants.JENKINS_USERNAME, constants.JENKINS_API_TOKEN)
      .on('response', (response) => {
        if (response.statusCode > 299) {
          onJenkinsHttpError(`Jenkins status code ERROR: ${response.statusCode}`);
          resolve([]);
          return;
        }

        let body = '';
        response.on('data', (chunk) => {
          body += chunk;
        });
        response.on('end', () => {
          resolve(helpers.normalizeParameters(JSON.parse(body).actions[0]));
        });
      })
      .on('error', (error) => {
        onJenkinsHttpError(error);
        resolve([]);
      });
  });
}

function getAllParameters() {
  const arm_params = getParameters(ARM_CONTAINERIZED_PARAMETERS_QUERY);
  const ark_params = getParameters(ARK_CONTAINERIZED_PARAMETERS_QUERY);
  const web_test_params = getParameters(WEB_TEST_PARAMETERS_QUERY);
  const python_test_params = getParameters(NEW_PYTHON_3_TOX_TEST_PARAMETERS_QUERY);
  const rest_test_params = getParameters(REST_TEST_PARAMETERS_QUERY);

  return Promise.all([
    arm_params,
    ark_params,
    web_test_params,
    python_test_params,
    rest_test_params
  ]);
}

async function getParametersCollection() {
  const keys = {};
  let params = {};
  [params.arm, params.ark, params.web, params.python, params.rest] = await getAllParameters();


  build_keys.keys.arm = params.arm.map(param => param.name);
  build_keys.keys.ark = params.ark.map(param => param.name);
  build_keys.keys.web = params.web.map(param => param.name);
  build_keys.keys.python = params.python.map(param => param.name);
  build_keys.keys.rest = params.rest.map(param => param.name);

  // Common keys are keys needed for arm, ark, and maybe test builds
  keys.common = _.intersection(build_keys.keys.arm, build_keys.keys.ark);
  params.common = params.arm.filter(param => keys.common.includes(param.name));

  // Keys for each type that aren't covered in 'common'
  keys.arm = _.difference(build_keys.keys.arm, keys.common);
  keys.ark = _.difference(build_keys.keys.ark, keys.common);
  keys.web = _.difference(build_keys.keys.web, keys.common);
  keys.python = _.difference(build_keys.keys.python, keys.common);
  keys.rest = _.difference(build_keys.keys.rest, keys.common);

  // Filter for params specific to each type
  params = _.mapObject(params, (vals, key) => {
    const filtered = vals.filter(val => keys[key].includes(val.name));

    // Change default to an array holding the jenkins default if param
    // is a multi select option like MACHINE or PRODUCT.
    return filtered.map((val) => {
      const new_param = val;
      if (MULTI_SELECT_KEYS.includes(val.name)) {
        new_param.defaultParameterValue.value = [val.defaultParameterValue.value];
      }
      return new_param;
    });
  });

  // Add a custom boolean for whether or not to include the test builds
  params.web.unshift(getTestBuildBooleanParameter('web'));
  params.python.unshift(getTestBuildBooleanParameter('python'));
  params.rest.unshift(getTestBuildBooleanParameter('rest'));

  return params;
}

function haveParamsChanged(new_params) {
  const new_param_string = JSON.stringify(new_params);
  const old_params_string = JSON.stringify(build_parameters_collection || {});

  if (new_param_string !== old_params_string) return true;
  return false;
}

// -----------------------------------------------------------------------------
// exports
// -----------------------------------------------------------------------------
function changeFeed(io) {
  io.on('connection', (socket) => {
    socket.on('login', () => {
      getParametersCollection().then((params) => {
        if (haveParamsChanged(params)) {
          build_parameters_collection = params;
          io.sockets.emit('parameters', build_parameters_collection);
        } else {
          socket.emit('parameters', build_parameters_collection);
        }
      });
    });
  });
}

module.exports = {
  build_keys,
  changeFeed
};
