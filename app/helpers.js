const _ = require('underscore');
const { URL } = require('url');
const constants = require('./server-constants');

// This isn't data driven. Depends on the order passed to Promise.all in builds.js#getSomeBuilds.
const BUILD_TYPE_ORDER = ['arm', 'arm', 'ark', 'test', 'test', 'test'];

function flattenBuilds(buildData) {
  const all_builds = [];

  // Iterate each type of build (arm-contain.., arm-param.., ark, test-web, test-python).
  buildData.forEach((buildsJSON, typeIdx) => {
    const builds = JSON.parse(buildsJSON);

    // Different types of builds are returned by Jenkins under different keys.
    const builds_key = builds.allBuilds ? 'allBuilds' : 'builds';
    const build_type = BUILD_TYPE_ORDER[typeIdx];

    let test_type;

    switch (typeIdx) {
      case 3:
        test_type = 'Web Test';
        break;
      case 4:
        test_type = 'Python Test';
        break;
      case 5:
        test_type = 'Rest Test';
        break;
      default:
        break;
    }

    if (builds[builds_key]) {
      // Iterate each build of this type.
      builds[builds_key].forEach((build) => {
        const flat_build = {};
        const actions = build.actions;
        flat_build.type = build_type;

        // build number isn't unique across ark/arm/test. build id is just a time stamp so this
        // also can't be counted on for uniqueness. Combining the two should be good.
        flat_build.id = `${build.id}${build.number}`;

        // Combine and omit 'id' and 'number' since neither are unique enough across build types
        _.extend(flat_build, _.omit(build, 'actions', 'artifacts', 'id', 'number'));

        if (build.artifacts) {
          flat_build.artifacts = build.artifacts.filter(artifact => artifact.fileName.endsWith('p7m'));
        }

        if (actions) {
          if (actions[0] && actions[0].parameters) {
            actions[0].parameters.forEach((param) => {
              flat_build[param.name] = param.value;
            });
          }

          if (actions[1] && actions[1].causes) {
            flat_build.userId = actions[1].causes[0].userId;
            flat_build.userName = actions[1].causes[0].userName;
          }

          if (actions[4] && actions[4].lastBuiltRevision) {
            flat_build.SHA = actions[4].lastBuiltRevision.SHA1;
          } else if (actions[5] && actions[5].lastBuiltRevision) {
            flat_build.SHA = actions[5].lastBuiltRevision.SHA1;
          }
        }

        // Store ARK build PRODUCT and test type in MACHINE for front-end simplicity.
        if (test_type) flat_build.MACHINE = test_type;
        if (flat_build.PRODUCT) flat_build.MACHINE = flat_build.PRODUCT;

        all_builds.push(flat_build);
      });
    }
  });

  return all_builds;
}

function normalizeParameters(params) {
  const parameters = [];

  _.each(params.parameterDefinitions, (e) => {
    parameters.push(e);
  });

  return parameters;
}

function stringifyParameters(params) {
  return _.reduce(params, (memo, val, key) => {
    const current_value = typeof val === 'string' ? val.trim() : val;
    const encoded_value = encodeURIComponent(current_value);
    return `${memo}${key}=${encoded_value}&`;
  }, '');
}

function separateMachines(params) {
  const splitParams = _.pick(params, constants.SPLIT_PARAMS);
  splitParams.params = _.omit(params, constants.SPLIT_PARAMS);
  return splitParams;
}

function toTitleCase(s) {
  const RE_TITLECASE = /\w\S*/g;
  return s.toString().replace(
    RE_TITLECASE,
    word => word.charAt(0).toUpperCase() + word.substr(1)
  );
}

function parseBuildStatus(statusCode, buildUrl) {
  const status = {};
  const url = new URL(buildUrl);

  if (url.pathname.includes('test-web-unit')) {
    status.build = 'Web Test';
  } else if (url.pathname.includes('python')) {
    status.build = 'Python Test';
  } else if (url.pathname.includes('Regression')) {
    status.build = 'REST Test';
  } else {
    status.build = url.searchParams.get('MACHINE') || url.searchParams.get('PRODUCT');
  }

  status.created = statusCode === 201;

  return status;
}

// -----------------------------------------------------------------------------
// Exports
// -----------------------------------------------------------------------------
module.exports = {
  flattenBuilds,
  normalizeParameters,
  stringifyParameters,
  separateMachines,
  toTitleCase,
  parseBuildStatus
};
