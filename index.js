require('babel-polyfill');
const constants = require('./app/server-constants');
const server = require('./app/server');

const host = process.env.HOST || '0.0.0.0';
const port = process.env.PORT || constants.DEFAULT_PORT;

server.listen(port, host);

console.log(`Server now listening on port ${port}`);
